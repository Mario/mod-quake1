#!/bin/bash

CONVERT=convert
OUT_DIR=out
IN_DIR=textures
SIZE_DIR=simple


# Resize images in the textures directory to match their counterparts in simple
# Synopsis: scale_image
function scale_image()
{
	img_w=$(identify -format %w "$SIZE_DIR/$1.jpg")
	img_h=$(identify -format %h "$SIZE_DIR/$1.jpg")
	local scale="${img_w}x${img_h}"
	convert "$IN_DIR/$1.jpg"       -resize "$scale!" "$OUT_DIR/$1.jpg"
}

# Create the files for packaging
function generate()
{
	#local SRC_FILES=$(ls $IN_DIR/*.jpg)
	local SRC_FILES=$(find $IN_DIR -type f '!' -iname ".*" )
	for src_file in $SRC_FILES
	do
		local jpeg_base=$(basename $src_file .jpg)

		echo -e "Resizing \x1b[1m$src_file\x1b[0m"
		scale_image "$jpeg_base"
	done
}


mkdir -p "$OUT_DIR"

generate
