/*
Generated header file for goat
This file contains frame number definitions for use in code referencing the model, to make code more readable and maintainable.
*/

#ifndef MODEL_GOAT_H
#define MODEL_GOAT_H

#define MODEL_GOAT_CUBE.002_0 0
#define MODEL_GOAT_CUBE.002_START 0
#define MODEL_GOAT_CUBE.002_END 1
#define MODEL_GOAT_CUBE.002_LENGTH 1
#define MODEL_GOAT_CUBE.002_FRAMERATE 10

#define MODEL_GOAT_ARMATUREACTION.002_0 0
#define MODEL_GOAT_ARMATUREACTION.002_START 0
#define MODEL_GOAT_ARMATUREACTION.002_END 1
#define MODEL_GOAT_ARMATUREACTION.002_LENGTH 1
#define MODEL_GOAT_ARMATUREACTION.002_FRAMERATE 0.01

#endif /*MODEL_GOAT_H*/
