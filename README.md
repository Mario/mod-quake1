# mod-quake1
Assets for the Xonotic Quake 1 mod


Content in this package is of mixed licenses, but is all available freely online (with the possible exception of the mindgrind pack, which asks users to refrain from publishing this with anything but Quake and Quake mods)
